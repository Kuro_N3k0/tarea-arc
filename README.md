# Sistemas Operativos

## Nombre
Alvarez Garcia Antonio Cesar

## [Descripcion de tarea](https://gitlab.com/SistemasOperativos-Ciencias-UNAM/sistemasoperativos-ciencias-unam.gitlab.io/blob/master/tareas/tarea-arch.md)

## Componentes del equipo
* CPU: Intel(R) Core(TM) i7-6500U CPU @ 2.50GHz
* Memoria RAM: Micron 8GiB SODIMM DDR3 Síncrono 1600 MHz
* Disco duro: ATA Disk TOSHIBA MQ01ABD1 931GiB (1TB)
* Tarjetas de red: Realtek Semiconductor Co., Ltd. 33MHz  64 bits
* dispositivos: USB controller, Display controller Sun XT [Radeon HD 8670A/8670M/8690M / R5 M330]

## lspci
- El comando lspci lsita todos los componentes titpo pci (Peripheral Component Interconnec) como son las tarjetas de red, tarjetas de sonido o tarjetas de video.
* <b>-v:</b> se utiliza para informacion mas explicita y que muestre la informacion mas detallada acerca de los dispositivos.
* <b>-vv:da</b> informacion aun mas detallada que -v
* <b>-t:</b> Nos ayuda mostrando un diagrama de arbol con la informacion

## lsusb
- El comando lsusb nos ayuda mostrando informacion acerca de los autobuses USB en el sistema y los dispositivos conectados a ellos
* <b>-t:</b> Se utiliza para mostrar la jerarquía de los dispositivos USB como un árbol.
* <b>-v:</b> Nos da información sobre la version en salida estandar.


## lsblk
- El camando enlista la informacion sobre todos o los bloques especificados.
* <b>--all:</b> No enlista dispositivos vacios por defecto
* <b>--fs:</b> Nos da informacion sobre los filesystems
* <b>--perms:</b>Nos da informacion sobre el propietario del dispositivo de salida, y el modo de grupo.
* <b>--paths:</b> Nos da las rutas completas del dispositivo.

## lshw
- Funciona para extraer información detallada sobre la configuración de hardware de la máquina.
* <b>-html:</b> Da salida al árbol de dispositivos como una página HTML.
* <b>-short:</b> Muestra el arbol de rutas de hardware de dispositivos.
* <b>-businfo:</b> Da salida a la lista de dispositivos que muestra la informacion del bus, que detalla las direcciones SCSI, USB, IDE y PCI

## smartctl
- Controla el Self-Monitoring, el sistema de analisis e informes de unidades de disco duro.
 Su proposito es controlar la fiabilidad de la unidad de disco duro y predecir fallos en este, para llevar acabo diferentes tipos de unidad de autopruebas.
* <b>--scan:</b> Analiza en busca de dispositivos y copias de cada nombre de dispositivo, tipo de dispositivo y de protocolo ([ATA] o [SCSI])
* <b>--all:</b> Muestra toda la informacion SMART acerca del disco, o informacion sobre la unidad de cinta
* <b>--xall:</b> Muestra toda la informacion SMART y no-SMART del disco.
* <b>/dev/sda:</b> Es el primer disco duro detectado.








